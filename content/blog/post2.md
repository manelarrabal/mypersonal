---
title: "1984 - George Orwell"
date: 2021-05-16T15:06:41+06:00
image: "images/1984.jpeg"
featured: false
categories: ["Libros"]
draft: false
---

Quizás tenía unas expectativas desmesuradas para este libro, en Internet hay un sinfín de críticas positivas pero no ha calado fondo en mi pensamiento. Estamos viviendo un momento similar y distópico con la covid, por lo que existen ciertos paralelismos con el libro. 

Lo verdaderamente apasionante del libro es la imaginació que tiene el escritor George Orwell para crear-se ese mundo donde toda la sociedad esta controlada por pantallas nombradas Gran Hermano. Si, de ahí viene el nombre de esa basura de programa....

El libro nos describe una sociedad autoritaria donde los gobiernos controlan todos los pensamientos que tienen los protagonistas, y muestra los recursos que destina el gobierno para ensombrecer el aprendizaje y la comprensión de la lengua. La idea del gobierno es hacer una población que se puede controlar con facilidad y eso lo logra sometiendo al pueblo, y creando un nuevo lenguaje llamado "nuevalengua".

NuevaLengua es un lenguaje a donde se simplifican las cosas, por ejemplo, para referir-nos en la cantidad de liquido que tenemos en un baso podemos utilitzar adjetivos como "mucho, bastante, suficiente, poco, poquísimo, etc" en cambio, en nueva lengua solo existe muchamucha agua, muchaagua, pocapoca agua, poca agua. 

![img](/images/orwell.jpg)
