---
title: "GRANADA Y SUS ENCANTOS"
date: 2021-11-01T11:06:48+06:00
image: "images/011.jpg"
featured: true
categories: ["Arte y Cultura"]
draft: false
---

La **Alhambra**, Fortaleza llena de historia y cultura Al-Andalusí. Moldeada desde sus inicios por innumerables palacios de la dinastía nazarí, se encuentra pertrechada en la colina Sabika. Visita obligada en Granada, entre sus rincones nos podemos perder en el mirador de San Nicolás, dónde tenemos estás bonitas vistas entre cantautores y guitarristas.


![img](images/blade.jpeg)
