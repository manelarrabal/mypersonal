---
title: "Gregario - Charly Wegelius"
date: 2021-07-14T15:06:48+06:00
image: "images/gregario2.jpeg"
featured: false
categories: ["Libros"]
draft: false
---

A mí no me representa son las primeras palabras que pienso al recordar la lectura de Gregario.

El libro está bien escrito y de simple lectura, pero como amante del ciclismo me parece un libro autobiográfico un poco infumable. Es evidente que la profesión del gregario no siempre será la más agradecida, sin embargo describe su carrera como una tortura y sin aparentes buenos momentos.

Él tal “Charlio” describe sus años como profesional de la bicicleta y no despierta ninguna aspiración o motivación para superarse. Llegar a ser gregario debe de ser grandioso, no obstante siempre hay que intentar aspirar a más. El susodicho solo reflexiona con las cosas negativas de su profesión y demuestra desde el principio, hasta el final del libro lo que realmente fue, **un gran perdedor**.

![img](/images/gregario.jpg)
