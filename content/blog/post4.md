---
title: "La noche te pesa y te convierte en presa"
date: 2021-07-15T15:40:55+06:00
image: "images/01.jpg"
featured: true
categories: ["Informatica"]
draft: true
---


Uso del hugo:

+ Podemos probar en local el proyecto con **hugo server**
+ Creamos la carpeta public con **hugo** antes de subir el proyecto a online
+ en baseUrl hay que poner la ruta del gitlab!




1. Primero vamos a Gitlab y luego creamos un nuevo proyecto. 
2. Copiamos el proyecto en el PC personal. con **git clone...**
3. Una vez tengamos nuevos archivos, hacemos **git add .**
4. Modificamos el proyecto, y cuando nos iterese guardar un punto: **commit -m "ejemplo"**
5. Subimos el proyecto a Internet con **git push**
